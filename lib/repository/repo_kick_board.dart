import 'package:daligo_admin_app/config/config_api.dart';
import 'package:daligo_admin_app/functions/token_lib.dart';
import 'package:daligo_admin_app/model/common_result.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_list_result.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_request.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_result.dart';
import 'package:daligo_admin_app/model/kick_board_update_request.dart';
import 'package:daligo_admin_app/model/statistics_result.dart';
import 'package:dio/dio.dart';

class RepoKickBoard {
  Future<CommonResult> setKickBoard(KickBoardRequest request) async {
    const String baseUrl = '$apiUri/kick-board/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<KickBoardListResult> getList({int page = 1}) async {
    const String baseUrl = '$apiUri/kick-board/all';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return KickBoardListResult.fromJson(response.data);
  }

  Future<KickBoardResult> getKickBoard(int id) async {
    const String baseUrl = '$apiUri/kick-board/kick-board?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return KickBoardResult.fromJson(response.data);
  }

  Future<CommonResult> putKickBoard(int id, KickBoardUpdateRequest request) async {
    const String baseUrl = '$apiUri/kick-board/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl.replaceAll('{id}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<StatisticsResult> getStatistics(String dateStart, String dateEnd) async {
    const String baseUrl = '$apiUri/kick-board-history/stats/total-price?dateEnd={dateEnd}&dateStart={dateStart}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl.replaceAll('{dateStart}', dateStart).replaceAll('{dateEnd}', dateEnd),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return StatisticsResult.fromJson(response.data);
  }
}