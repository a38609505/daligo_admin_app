import 'package:dio/dio.dart';
import 'package:daligo_admin_app/config/config_api.dart';
import 'package:daligo_admin_app/model/auth_check_request.dart';
import 'package:daligo_admin_app/model/auth_send_request.dart';
import 'package:daligo_admin_app/model/common_result.dart';

class RepoPhoneAuth {
  Future<CommonResult> doSend(AuthSendRequest request) async {
    const String baseUrl = '$apiUri/auth-phone/auth/start';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doCheck(AuthCheckRequest request) async {
    const String baseUrl = '$apiUri/auth-phone/auth/end';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false
        )
    );

    return CommonResult.fromJson(response.data);
  }
}