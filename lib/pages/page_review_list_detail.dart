import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/review/review_result.dart';
import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:daligo_admin_app/pages/page_review_update.dart';
import 'package:daligo_admin_app/repository/repo_review.dart';
import 'package:flutter/material.dart';

class PageReviewListDetail extends StatefulWidget {
  const PageReviewListDetail({super.key, required this.id, required this.title});

  final int id;
  final String title;

  @override
  State<PageReviewListDetail> createState() => _PageReviewListDetailState();
}

class _PageReviewListDetailState extends State<PageReviewListDetail> {
  String name = "";
  String content = "";
  String imgName = "";
  String dateWrite = "";

  Future<void> _getReview() async {
    ReviewResult result = await RepoReview().getReview(widget.id);

    setState(() {
      name = result.data.name;
      content = result.data.content;
      imgName = result.data.imgName;
      dateWrite = result.data.dateWrite;
    });
  }

  Future<void> _delReview() async {
    await RepoReview().delReview(widget.id).then((res) {
      ComponentNotification(
        success: true,
        title: '후기 삭제 완료',
        subTitle: '삭제가 완료되었습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {

      ComponentNotification(
        success: false,
        title: '후기 삭제 실패',
        subTitle: '다시 시도해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    super.initState();
    _getReview();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: widget.title,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: colorLightGray),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    name,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    dateWrite,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Text(
                content,
                style: const TextStyle(
                  fontSize: fontSizeBig,
                  wordSpacing: 1.5,
                  height: 1.3,
                ),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Image.network(imgName),
            ],
          ),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: ComponentTextBtn("수정", () { Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      PageReviewUpdate(id: widget.id)));
          }),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: ComponentTextBtn("삭제", () { _delReview(); }),
        ),
      ],
    );
  }
}
