import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/kick_board_update_request.dart';
import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:daligo_admin_app/repository/repo_kick_board.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageKickBoardUpdate extends StatefulWidget {
  const PageKickBoardUpdate({super.key, required this.id});

  final int id;

  @override
  State<PageKickBoardUpdate> createState() => _PageKickBoardUpdateState();
}

class _PageKickBoardUpdateState extends State<PageKickBoardUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putKickBoard(KickBoardUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickBoard().putKickBoard(widget.id, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '킥보드 수정 완료',
        subTitle: '킥보드 수정이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '킥보드 수정 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '킥보드 수정',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'posX',
                    decoration: StyleFormDecoration().getInputDecoration('위도'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'posY',
                    decoration: StyleFormDecoration().getInputDecoration('경도'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderFieldDecoration<bool>(
                    name: 'isUse',
                    decoration: StyleFormDecoration().getSwitchDecoration(),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                    ]),
                    initialValue: true,
                    builder: (FormFieldState<bool?> field) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          errorText: field.errorText,
                        ),
                        child: SwitchListTile(
                          title: const Text('킥보드 사용 여부'),
                          onChanged: field.didChange,
                          value: field.value ?? true,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('수정', () {
            if(_formKey.currentState!.saveAndValidate()) {
              KickBoardUpdateRequest request = KickBoardUpdateRequest(
                double.parse(_formKey.currentState!.fields['posX']!.value),
                double.parse(_formKey.currentState!.fields['posY']!.value),
                _formKey.currentState!.fields['isUse']!.value,
              );
              _putKickBoard(request);
            }
          }),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}

