import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_logo.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/pages/page_find_password.dart';
import 'package:daligo_admin_app/pages/page_find_username.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_section_title.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/functions/token_lib.dart';
import 'package:daligo_admin_app/middleware/middleware_login_check.dart';
import 'package:daligo_admin_app/model/login_request.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarLogo(
        "logo_white.png",
        () {},
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 200,
              color: Colors.white,
              child: Image.asset('assets/main_img.png', fit: BoxFit.cover),
            ),
          ],
        ),
        const ComponentSectionTitle('관리자 로그인이 필요합니다.'),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'username',
                    decoration: StyleFormDecoration().getInputDecoration('아이디'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'password',
                    decoration: StyleFormDecoration().getInputDecoration('비밀번호'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('로그인', () {
            if(_formKey.currentState!.saveAndValidate()) {
              LoginRequest loginRequest = LoginRequest(
                  _formKey.currentState!.fields['username']!.value,
                  _formKey.currentState!.fields['password']!.value,
              );
              _doLogin(loginRequest);
            }
          }),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingLeftRight,
          child: ElevatedButton(
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageFindUsername())),
            child: Text("아이디 찾기"),
            style: ElevatedButton.styleFrom(
              primary: colorPrimary,
              onPrimary: Colors.white,
              padding: contentPaddingButton,
              elevation: buttonElevation,
              textStyle: const TextStyle(
                fontSize: fontSizeMid,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(buttonRadius),
                side: BorderSide(
                  color: colorPrimary,
                )
              )
            ),
          ),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingLeftRight,
          child: ElevatedButton(
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageFindPassword())),
            child: Text("비밀번호 찾기"),
            style: ElevatedButton.styleFrom(
                primary: colorPrimary,
                onPrimary: Colors.white,
                padding: contentPaddingButton,
                elevation: buttonElevation,
                textStyle: const TextStyle(
                  fontSize: fontSizeMid,
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(buttonRadius),
                    side: BorderSide(
                      color: colorPrimary,
                    )
                )
            ),
          ),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}
