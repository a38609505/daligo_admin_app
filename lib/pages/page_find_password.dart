import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/model/auth_check_request.dart';
import 'package:daligo_admin_app/model/auth_send_request.dart';
import 'package:daligo_admin_app/model/find_password_request.dart';
import 'package:daligo_admin_app/pages/page_login.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:daligo_admin_app/repository/repo_phone_auth.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PageFindPassword extends StatefulWidget {
  const PageFindPassword({super.key});

  @override
  State<PageFindPassword> createState() => _PageFindPasswordState();
}

class _PageFindPasswordState extends State<PageFindPassword> {
  final _formKey = GlobalKey<FormBuilderState>();
  int? option;

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  String _authSendBtnName = '인증번호 전송';
  bool _isAuthNumSend = false; // 인증번호 보냈냐??
  bool _isAuthNumSuccess = false; // 인증 완료 했냐??

  Future<void> _doAuthSend(AuthSendRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doSend(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 전송',
        subTitle: '인증번호가 ${request.phoneNumber} 으로 전송되었습니다.',
      ).call();

      setState(() {
        _authSendBtnName = '인증번호 재전송';
        _isAuthNumSend = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 전송 실패',
        subTitle: '고객센터로 문의하세요.',
      ).call();
    });
  }

  Future<void> _doAuthCheck(AuthCheckRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doCheck(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 확인 완료',
        subTitle: '인증번호 확인이 완료되었습니다.',
      ).call();

      setState(() {
        _isAuthNumSuccess = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 확인 실패',
        subTitle: '인증번호를 정확히 입력해주세요.',
      ).call();
    });
  }

  Future<void> _findPassword(FindPasswordRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().findPassword(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '임시 비밀번호 발급 완료',
        subTitle: '회원님의 비밀번호로 임시 비밀번호를 전송하였습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '임시 비밀번호 발급 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '비밀번호 찾기',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'username',
                    decoration: StyleFormDecoration().getInputDecoration('아이디'),
                    maxLength: 30,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                      FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'phoneNumber',
                    inputFormatters: [maskPhoneNumber],
                    decoration: StyleFormDecoration().getInputDecoration('연락처'),
                    maxLength: 13,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                      FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                    ]),
                    enabled: !_isAuthNumSuccess,
                  ),
                ),
                const ComponentMarginVertical(),
                !_isAuthNumSuccess
                  ? Container(
                    child: ComponentTextBtn(
                      _authSendBtnName,
                      () {
                        String phoneNumberText = _formKey
                          .currentState!.fields['phoneNumber']!.value;
                        if (phoneNumberText.length == 13) {
                          AuthSendRequest authSendRequest = AuthSendRequest(
                          phoneNumberText,
                        );
                        _doAuthSend(authSendRequest);
                      }
                    },
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                    ),
                  )
                  : Container(),
                const ComponentMarginVertical(),
                _isAuthNumSend && !_isAuthNumSuccess
                  ? Container(
                    decoration: formBoxDecoration,
                    padding: bodyPaddingAll,
                    child: FormBuilderTextField(
                      name: 'authNumber',
                      decoration:
                      StyleFormDecoration().getInputDecoration('인증번호'),
                      maxLength: 6,
                      keyboardType: TextInputType.text,
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                        FormBuilderValidators.maxLength(6, errorText: formErrorMaxLength(6)),
                      ]),
                      enabled: _isAuthNumSend,
                    ),
                  )
                  : Container(),
                _isAuthNumSend && !_isAuthNumSuccess
                  ? Container(
                    child: ComponentTextBtn(
                      '인증번호 확인',
                      () {
                        String phoneNumberText = _formKey.currentState!.fields['phoneNumber']!.value;
                        String authNumberText = _formKey.currentState!.fields['authNumber']!.value;

                        if (phoneNumberText.length == 13 && authNumberText.length == 6) {
                          AuthCheckRequest authCheckRequest =
                          AuthCheckRequest(
                          phoneNumberText,
                          authNumberText,
                          );
                          _doAuthCheck(authCheckRequest);
                        }
                      },
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                    ),
                  )
                  : Container(),
                const ComponentMarginVertical(),
                _isAuthNumSend && _isAuthNumSuccess
                ? Container(
                  child: ComponentTextBtn('비밀번호 확인', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      FindPasswordRequest request = FindPasswordRequest(
                        _formKey.currentState!.fields['username']!.value,
                        _formKey.currentState!.fields['name']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                      );
                      _findPassword(request);
                    }
                  }),
                )
                : Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
