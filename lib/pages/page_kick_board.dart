import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_request.dart';
import 'package:daligo_admin_app/repository/repo_kick_board.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageKickBoard extends StatefulWidget {
  const PageKickBoard({super.key});

  @override
  State<PageKickBoard> createState() => _PageKickBoardState();
}

class _PageKickBoardState extends State<PageKickBoard> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  Future<void> _setKickBoard(KickBoardRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickBoard().setKickBoard(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '킥보드 등록 완료',
        subTitle: '킥보드 등록이 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '킥보드 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '킥보드 등록',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'modelName',
                    decoration: StyleFormDecoration().getInputDecoration('모델명'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(3, errorText: formErrorMinLength(3)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'uniqueNumber',
                    decoration: StyleFormDecoration().getInputDecoration('고유번호'),
                    maxLength: 6,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                      FormBuilderValidators.maxLength(6, errorText: formErrorMaxLength(6)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDropdown<String>(
                    name: 'priceBasis',
                    initialValue: 'BASIC',
                    decoration: const InputDecoration(
                      label: Text('기준요금'),
                    ),
                    onChanged: (value) {
                      setState(() {
                        option = value;
                      });
                    },
                    items: const [
                      DropdownMenuItem(value: "BASIC", child: Text('베이직')),
                      DropdownMenuItem(value: "PREMIUM", child: Text('프리미엄')),
                    ],
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateBuy",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("구입일"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'posX',
                    decoration: StyleFormDecoration().getInputDecoration('위도'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'posY',
                    decoration: StyleFormDecoration().getInputDecoration('경도'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('등록', () {
            if(_formKey.currentState!.saveAndValidate()) {
              KickBoardRequest request = KickBoardRequest(
                _formKey.currentState!.fields['modelName']!.value,
                _formKey.currentState!.fields['uniqueNumber']!.value,
                _formKey.currentState!.fields['priceBasis']!.value,
                _formKey.currentState!.fields['dateBuy']!.value.toString().substring(0, 10),
                double.parse(_formKey.currentState!.fields['posX']!.value),
                double.parse(_formKey.currentState!.fields['posY']!.value),
              );
              _setKickBoard(request);
            }
          }),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}
