import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/model/member/member_create_request.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PageMemberCreate extends StatefulWidget {
  const PageMemberCreate({super.key});

  @override
  State<PageMemberCreate> createState() => _PageMemberCreateState();
}

class _PageMemberCreateState extends State<PageMemberCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String? option;

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  var maskLicenseNumber = MaskTextInputFormatter(
      mask: '##-##-######-##',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  Future<void> _setMember(MemberCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().setMember(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '회원등록 완료',
        subTitle: '회원등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '회원등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '회원 등록'),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingAll,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDropdown<String>(
                    name: 'memberGroup',
                    initialValue: 'ROLE_USER',
                    decoration: const InputDecoration(
                      label: Text('회원 그룹'),
                    ),
                    onChanged: (value) {
                      setState(() {
                        option = value;
                      });
                    },
                    items: const [
                      DropdownMenuItem(value: "ROLE_ADMIN", child: Text('관리자')),
                      DropdownMenuItem(value: "ROLE_USER", child: Text('사용자')),
                    ],
                  ),
                ),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'username',
                    decoration: StyleFormDecoration().getInputDecoration('아이디'),
                    maxLength: 30,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                      FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'password',
                    decoration: StyleFormDecoration().getInputDecoration('비밀번호'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'passwordRe',
                    decoration:
                        StyleFormDecoration().getInputDecoration('비밀번호 확인'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'phoneNumber',
                    decoration:
                        StyleFormDecoration().getInputDecoration('연락처'),
                    maxLength: 13,
                    keyboardType: TextInputType.text,
                    inputFormatters: [maskPhoneNumber],
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                      FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'licenseNumber',
                    decoration:
                    StyleFormDecoration().getInputDecoration('면허증 번호'),
                    maxLength: 15,
                    keyboardType: TextInputType.text,
                    inputFormatters: [maskLicenseNumber],
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(
                          errorText: formErrorRequired),
                      FormBuilderValidators.minLength(15, errorText: formErrorMinLength(15)),
                      FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateBirth",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("생년월일"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn('회원등록', () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      MemberCreateRequest request = MemberCreateRequest(
                        _formKey.currentState!.fields['memberGroup']!.value,
                        _formKey.currentState!.fields['username']!.value,
                        _formKey.currentState!.fields['password']!.value,
                        _formKey.currentState!.fields['passwordRe']!.value,
                        _formKey.currentState!.fields['name']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                        _formKey.currentState!.fields['licenseNumber']!.value,
                        _formKey.currentState!.fields['dateBirth']!.value.toString().substring(0, 10)
                      );
                      _setMember(request);
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
