import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_result.dart';
import 'package:daligo_admin_app/pages/page_kick_board_update.dart';
import 'package:daligo_admin_app/repository/repo_kick_board.dart';
import 'package:flutter/material.dart';

class PageKickBoardListDetail extends StatefulWidget {
  const PageKickBoardListDetail({super.key, required this.id, required this.title});

  final int id;
  final String title;

  @override
  State<PageKickBoardListDetail> createState() => _PageKickBoardListDetailState();
}

class _PageKickBoardListDetailState extends State<PageKickBoardListDetail> {
  String kickBoardStatus = "";
  String dateBuy = "";
  double posX = 0;
  double posY = 0;
  String isUse = "";

  Future<void> _getKickBoard() async {
    KickBoardResult result = await RepoKickBoard().getKickBoard(widget.id);

    setState(() {
      kickBoardStatus = result.data.kickBoardStatus;
      dateBuy = result.data.dateBuy;
      posX = result.data.posX;
      posY = result.data.posY;
      isUse = result.data.isUse;
    });
  }

  @override
  void initState() {
    super.initState();
    _getKickBoard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: widget.title,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: colorLightGray),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "상태",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    kickBoardStatus,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "구입일",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    dateBuy,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "위도",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    "$posX",
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "경도",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    "$posY",
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "사용여부",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                  Text(
                    isUse,
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      wordSpacing: 1.5,
                      height: 1.3,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        const ComponentMarginVertical(),
        Container(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: ComponentTextBtn("수정", () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        PageKickBoardUpdate(id: widget.id)));
          }),
        ),
      ],
    );
  }
}
