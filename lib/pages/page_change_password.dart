import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/change_password_request.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageChangePassword extends StatefulWidget {
  const PageChangePassword({super.key});

  @override
  State<PageChangePassword> createState() => _PageChangePasswordState();
}

class _PageChangePasswordState extends State<PageChangePassword> {
  final _formKey = GlobalKey<FormBuilderState>();
  int? option;

  Future<void> _changePassword(ChangePasswordRequest passwordRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().changePassword(passwordRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '비밀번호 변경 완료',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '비밀번호 변경 실패',
        subTitle: '비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '비밀번호 변경',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'currentPassword',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('현재 비밀번호'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'changePassword',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('새로운 비밀번호'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'changePasswordRe',
                    obscureText: true,
                    decoration: StyleFormDecoration().getInputDecoration('새로운 비밀번호 확인'),
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('수정', () {
            if (_formKey.currentState!.saveAndValidate()) {
              ChangePasswordRequest request = ChangePasswordRequest(
                _formKey.currentState!.fields['currentPassword']!.value,
                _formKey.currentState!.fields['changePassword']!.value,
                _formKey.currentState!.fields['changePasswordRe']!.value,
              );
              _changePassword(request);
            }
          }),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}
