import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/statistics_result.dart';
import 'package:daligo_admin_app/repository/repo_kick_board.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageStatistics extends StatefulWidget {
  const PageStatistics({Key? key}) : super(key: key);

  @override
  State<PageStatistics> createState() => _PageStatisticsState();
}

class _PageStatisticsState extends State<PageStatistics> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _isClickButton = false;
  num totalPrice = 0;

  Future<void> _getStatistics(String dateStart, String dateEnd) async {
    StatisticsResult result = await RepoKickBoard().getStatistics(dateStart, dateEnd);

    setState(() {
      totalPrice = result.data.totalPrice;
      _isClickButton = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: "매출 통계"),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: bodyPaddingAll,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateStart",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("시작일"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderDateTimePicker(
                    name: "dateEnd",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    decoration: StyleFormDecoration().getDateBoxDecoration("종료일"),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  child: ComponentTextBtn('확인', () {
                    _getStatistics(_formKey.currentState!.fields['dateStart']!.value.toString().substring(0, 10), _formKey.currentState!.fields['dateEnd']!.value.toString().substring(0, 10));
                  }),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.big,
                ),
                _isClickButton
                ? Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "기간: ",
                              style: TextStyle(
                                fontSize: fontSizeBig
                              ),
                            ),
                            Text(
                              "${_formKey.currentState!.fields['dateStart']!.value.toString().substring(0, 10)} ~ ${_formKey.currentState!.fields['dateEnd']!.value.toString().substring(0, 10)}",
                              style: const TextStyle(
                                fontSize: fontSizeBig
                              ),
                            ),
                          ],
                        ),
                        const ComponentMarginVertical(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "총 매출은 ",
                              style: TextStyle(
                                  fontSize: fontSizeBig
                              ),
                            ),
                            Text(
                              totalPrice.toString(),
                              style: const TextStyle(
                                  fontSize: fontSizeBig
                              ),
                            ),
                            const Text(
                              " 입니다.",
                              style: TextStyle(
                                  fontSize: fontSizeBig
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  )
                  : Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}