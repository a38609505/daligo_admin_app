import 'package:daligo_admin_app/components/common/component_appbar_logo.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/pages/page_kick_board_list.dart';
import 'package:daligo_admin_app/pages/page_member_create.dart';
import 'package:daligo_admin_app/pages/page_kick_board.dart';
import 'package:daligo_admin_app/pages/page_member_list.dart';
import 'package:daligo_admin_app/pages/page_my.dart';
import 'package:daligo_admin_app/pages/page_review.dart';
import 'package:daligo_admin_app/pages/page_review_list.dart';
import 'package:daligo_admin_app/pages/page_statistics.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarLogo(
        "logo_white.png",
        () {},
      ),
      body: ListView(
        children: [
          const ComponentMarginVertical(),
          Container(
            padding: const EdgeInsets.only(
              bottom: 10,
              left: 20,
              right: 20,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageKickBoard())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "킥보드 등록",
                            style: TextStyle(
                              fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageKickBoardList())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "킥보드 리스트",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberCreate())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "회원 등록",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberList())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "회원 리스트",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageReview())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "후기 등록",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageReviewList())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "후기 리스트",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageStatistics())),
                        style: ElevatedButton.styleFrom(
                            primary: colorLightGray,
                            onPrimary: Colors.black,
                            padding: contentPaddingButton,
                            elevation: buttonElevation,
                            textStyle: const TextStyle(
                              fontSize: fontSizeMid,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                                side: const BorderSide(
                                  color: colorLightGray,
                                )
                            )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "매출 통계",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.44,
                      child: ElevatedButton(
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMy())),
                        style: ElevatedButton.styleFrom(
                          primary: colorLightGray,
                          onPrimary: Colors.black,
                          padding: contentPaddingButton,
                          elevation: buttonElevation,
                          textStyle: const TextStyle(
                            fontSize: fontSizeMid,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(buttonRadius),
                              side: const BorderSide(
                                color: colorLightGray,
                              )
                          )
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15),
                          child: Text(
                            "나의 정보",
                            style: TextStyle(
                                fontSize: fontSizeSuper
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      // drawer: Drawer(
      //   child: ListView(
      //     padding: const EdgeInsets.all(20),
      //     children: [
      //       const DrawerHeader(
      //         child: Row(
      //           children: [
      //             CircleAvatar(radius: 30,
      //               backgroundColor: Colors.blue,
      //             ),
      //             SizedBox(
      //               width: 15,
      //             ),
      //             Text("고달리",
      //               style: TextStyle(
      //                 fontWeight: FontWeight.w700,
      //                 fontSize: 24,
      //               ),
      //             ),
      //           ],
      //         ),
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '킥보드 등록',
      //           style: TextStyle(
      //             fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageKickBoard()));
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '킥보드 리스트',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '회원 등록',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberCreate()));
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '회원 리스트',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '후기 등록',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageReview()));
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '후기 리스트',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '매출 통계',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageStatistics()));
      //         },
      //       ),
      //       ListTile(
      //         title: const Text(
      //           '나의 정보',
      //           style: TextStyle(
      //               fontSize: fontSizeBig
      //           ),
      //         ),
      //         onTap: () {
      //         },
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
