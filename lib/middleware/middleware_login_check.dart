import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:flutter/material.dart';
import 'package:daligo_admin_app/functions/token_lib.dart';
import 'package:daligo_admin_app/pages/page_login.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();

    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()), (route) => false);
    }
  }
}
