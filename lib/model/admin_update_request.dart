class AdminUpdateRequest {
  String name;
  String phoneNumber;

  AdminUpdateRequest(this.name, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['phoneNumber'] = phoneNumber;

    return data;
  }
}