class ReviewResponse {
  String name;
  String content;
  String imgName;
  String dateWrite;

  ReviewResponse(this.name, this.content, this.imgName, this.dateWrite);

  factory ReviewResponse.fromJson(Map<String, dynamic> json) {
    return ReviewResponse(
      json['name'],
      json['content'],
      json['imgName'],
      json['dateWrite'],
    );
  }
}