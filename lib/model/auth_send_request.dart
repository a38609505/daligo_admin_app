class AuthSendRequest {
  String phoneNumber;

  AuthSendRequest(this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['phoneNumber'] = phoneNumber;

    return data;
  }
}