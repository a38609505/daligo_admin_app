class MemberResponse {
  String memberGroup;
  String username;
  String name;
  String phoneNumber;
  String licenseNumber;
  double point;
  String dateCreate;
  String isEnabled;

  MemberResponse(this.memberGroup, this.username, this.name, this.phoneNumber, this.licenseNumber, this.point, this.dateCreate, this.isEnabled);

  factory MemberResponse.fromJson(Map<String, dynamic> json) {
    return MemberResponse(
        json['memberGroup'],
        json['username'],
        json['name'],
        json['phoneNumber'],
        json['licenseNumber'],
        json['point'],
        json['dateCreate'],
        json['isEnabled'],
    );
  }
}