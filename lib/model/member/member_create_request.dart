class MemberCreateRequest {
  String memberGroup;
  String username;
  String password;
  String passwordRe;
  String name;
  String phoneNumber;
  String licenseNumber;
  String dateBirth;

  MemberCreateRequest(this.memberGroup, this.username, this.password, this.passwordRe, this.name, this.phoneNumber, this.licenseNumber, this.dateBirth);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['memberGroup'] = memberGroup;
    data['username'] = username;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['licenseNumber'] = licenseNumber;
    data['dateBirth'] = dateBirth;

    return data;
  }
}