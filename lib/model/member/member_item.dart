class MemberItem {
  int id;
  String username;
  String memberGroup;
  String name;

  MemberItem(this.id, this.username, this.memberGroup, this.name);

  factory MemberItem.fromJson(Map<String, dynamic> json) {
    return MemberItem(
      json['id'],
      json['username'],
      json['memberGroup'],
      json['name'],
    );
  }
}