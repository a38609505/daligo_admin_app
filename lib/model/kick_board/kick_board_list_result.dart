import 'package:daligo_admin_app/model/kick_board/kick_board_item.dart';

class KickBoardListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<KickBoardItem>? list;

  KickBoardListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory KickBoardListResult.fromJson(Map<String, dynamic> json) {
    return KickBoardListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => KickBoardItem.fromJson(e)).toList() : [],
    );
  }
}