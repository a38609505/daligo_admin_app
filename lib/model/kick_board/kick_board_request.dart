class KickBoardRequest {
  String modelName;
  String uniqueNumber;
  String priceBasis;
  String dateBuy;
  double posX;
  double posY;

  KickBoardRequest(this.modelName, this.uniqueNumber, this.priceBasis, this.dateBuy, this.posX, this.posY);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['modelName'] = modelName;
    data['uniqueNumber'] = uniqueNumber;
    data['priceBasis'] = priceBasis;
    data['dateBuy'] = dateBuy;
    data['posX'] = posX;
    data['posY'] = posY;

    return data;
  }
}