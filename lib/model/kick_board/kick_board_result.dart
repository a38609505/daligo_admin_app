import 'package:daligo_admin_app/model/kick_board/kick_board_response.dart';

class KickBoardResult {
  KickBoardResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  KickBoardResult(this.data, this.isSuccess, this.code, this.msg);

  factory KickBoardResult.fromJson(Map<String, dynamic> json) {
    return KickBoardResult(
        KickBoardResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}